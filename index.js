/* eslint-disable no-undef */
const http = require('http');
// const url = require("url");
const myModule = require('./dataForIndex');

const htmlData = myModule.htmlData;
const jsonData = myModule.jsonData;
const uuidData = myModule.uuidData;
// const jsonData = require("./dataForIndex");

const myServer = http.createServer((request, response) => {
  const parsedUrl = request.url;
  const pathnameArray = parsedUrl.split('/');
  console.log('New Server Started');

  try {
    if (request.url === '/html') {
      response.writeHead(200, { 'Content-Type': 'text/html' });
      response.end(htmlData);
    } else if (request.url === '/json') {
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.end(JSON.stringify(jsonData));
    } else if (request.url === '/uuid') {
      response.writeHead(200, { 'Content-Type': 'text/plain' });
      response.end(uuidData.uuid);
    } else if (pathnameArray[1] === 'status') {
      response.writeHead(Number(pathnameArray[2]), {
        'Content-Type': 'text/html',
      });
      response.end();
    } else if (pathnameArray[1] === 'delay') {
      setTimeout(
        () => {
          response.writeHead(200, { 'Content-Type': 'text/html' });
          response.end(
            `<h1>Page loaded after ${pathnameArray[2]} seconds!</h1>`,
          );
        },
        Number(pathnameArray[2]) * 1000,
      );
    } else {
      if (pathnameArray[1] === 404) {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end('Not Found');
      } else {
        response.writeHead(200, { 'Content-Type': 'text/html' });
        response.end(`<h3>Hi! Welcome to Anup HTTP server!</h3>`);
      }
    }
  } catch (err) {
    console.log('Error', err);
  }
});
myServer.listen(8000, () => {
  console.log('Server Started!');
});

// console.log(http.request());
