/* eslint-disable no-undef */
const htmlData = `<!DOCTYPE html>
    <html>
    <head>
    </head>
    <body>
        <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
        <p> - Martin Fowler</p>

    </body>
    </html>`;
const jsonData = {
  slideshow: {
    author: "Yours Truly",
    date: "date of publication",
    slides: [
      {
        title: "Wake up to WonderWidgets!",
        type: "all",
      },
      {
        items: [
          "Why <em>WonderWidgets</em> are great",
          "Who <em>buys</em> WonderWidgets",
        ],
        title: "Overview",
        type: "all",
      },
    ],
    title: "Sample Slide Show",
  },
};

const uuidData = {
  uuid: "14d96bb1-5d53-472f-a96e-b3a1fa82addd",
};
module.exports = { htmlData, jsonData, uuidData };
